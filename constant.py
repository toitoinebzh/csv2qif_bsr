#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 20 19:35:52 2019

@author: antoine
"""

import os

DIR = os.path.dirname(os.path.realpath(__file__)) + "/icons/"

icon_path = os.path.join(DIR, 'Gartoon_mimetypes_qif.svg')
pixmap_path = os.path.join(DIR, 'Gartoon_mimetypes_qif.svg')

bank_name = {"BSR" : "Boursorama", "N26" : "N26", "CA56" : "Crédit Agricole du Morbihan", "CMB" : "Crédit Mutuel de Bretagne"}

icon_path_BSR = os.path.join(DIR, '320px-Boursorama.jpg')
icon_path_N26 = os.path.join(DIR, 'N26.png')
icon_path_CA56 = os.path.join(DIR, 'Logo_Credit_Agricole.png')
icon_path_CMB = os.path.join(DIR, '320px-Logo_Crédit_Mutuel_de_Bretagne_CMB.svg.png')

bank_picture = {"BSR" : icon_path_BSR, "N26" : icon_path_N26, "CA56" : icon_path_CA56, "CMB" : icon_path_CMB}

if __name__ == "__main__":
    for k,v in bank_name.items():
        print(k,v)

    for k,v in bank_picture.items():
        print(k,v)
