#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  2 21:35:36 2019

@author: antoine
"""

import sys
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget, QPushButton, QFileDialog, QMessageBox, QCheckBox, QComboBox
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import QSize

from csv_reader import Transactions
from constant import icon_path, pixmap_path, bank_name, bank_picture


class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        self.setWindowTitle("Csv2Qif")
        self.setWindowIcon(QIcon(icon_path))

        centralWidget = QWidget(self)
        self.setCentralWidget(centralWidget)

        gridLayout = QGridLayout(centralWidget)
        #centralWidget.setLayout(gridLayout)

        label = QLabel(self)
        pixmap = QPixmap(pixmap_path)
        label.setPixmap(pixmap)
        label.setAlignment(QtCore.Qt.AlignCenter)
        gridLayout.addWidget(label, 0, 0)

        title = QLabel(
            "Programme permettant de convertir des fichiers bancaires .csv en .qif de qualité",
            self)
        title.setAlignment(QtCore.Qt.AlignCenter)
        gridLayout.addWidget(title, 1, 0)

        self.comboBox = QComboBox(self)
        self.comboBox.setIconSize(QSize(100,30))
        self.comboBox.setEditable(True)
        self.comboBox.lineEdit().setReadOnly(True)
        self.comboBox.lineEdit().setAlignment(QtCore.Qt.AlignCenter)
        
        for bank_key, icon_path_bank in bank_picture.items():
            self.comboBox.addItem(QIcon(icon_path_bank), bank_name[bank_key])
        
        gridLayout.addWidget(self.comboBox, 2, 0)

        self.CBCat = QCheckBox("Inclure les catégories dans le fichier .qif")
        self.CBCat.setCheckState(QtCore.Qt.Unchecked)
        gridLayout.addWidget(self.CBCat, 3, 0)

        self.CBPayee = QCheckBox("Remplacer les tiers vides par le Mémo")
        self.CBPayee.setCheckState(QtCore.Qt.Checked)
        gridLayout.addWidget(self.CBPayee, 4, 0)
        
        button = QPushButton('Sélectionner le fichier .csv', self)
        button.clicked.connect(self.convert)
        gridLayout.addWidget(button, 5, 0)


    def convert(self):
        "realise la creation du fichier qif"
        fname = QFileDialog.getOpenFileName(self,
                                            "Sélectionnez le fichier .csv",
                                            "",
                                            "Fichier csv (*.csv ; *.CSV)")

        # filename
        # /home/antoine/Bureau/Qif/export-operations-02-01-2019_19-02-56.csv
        filename = fname[0]

        # on recupere la banque selectionnee
        bank = None
        combo_txt = self.comboBox.currentText()
        for key, name in bank_name.items() :
            if name == combo_txt :
                bank = key
        #print(bank)
                         
        try:
            transactions = Transactions(filename, bank)
            # print(transactions.get_qif())

            try:
                include_categorie = self.CBCat.isChecked()
                replace_empty_payee = self.CBPayee.isChecked()
                transactions.write_qif(include_categorie, replace_empty_payee)

                QMessageBox.information(
                    self,
                    "Succès !!",
                    "Le fichier <b>" +
                    transactions.qif_name +
                    "</b> a été créé avec succès :-) ")

            except BaseException:
                QMessageBox.warning(
                    self, "Erreur !!", "Erreurs lors de l'écriture du fichier .qif.")

        except BaseException:
            QMessageBox.warning(
                self,
                "Erreur !!",
                "Votre fichier n'est pas conforme aux attentes.")


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()
    sys.exit(app.exec_())
    

